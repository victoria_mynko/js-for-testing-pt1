//Task 5 - 5.1
const persons = [
  {name: "Oscar", age: 23},
  {name: "David", age: 15}
  ]


function compareAge() {
  if(persons[0].age > persons[1].age) {
      console.log(persons[0].name + " is older " + persons[1].name);
  }else if(persons[0].age < persons[1].age) {
      console.log(persons[0].name + " is younger " + persons[1].name)
  }else {
      console.log(persons[0].name + " is the same age as " + persons[1].name)
  }

}
compareAge();


//Task 5 - 5.2

const personAges = [
    {name: "Oscar", age: 23},
    {name: "David", age: 18},
    {name: "Robert", age: 28},
    {name: "Nicole", age: 24}
  ]
  
  personAges.sort()
  //personAges.sort(ageSortAsc)
  personAges.sort(ageSortDesc)
  
  function ageSortAsc(a, b){
    if(a.age < b.age)
        return 1
    if(a.age > b.age)
        return -1
    return 0
  }
  
  function ageSortDesc(a,b){
    if(a.age < b.age)
        return -1
    if(a.age > b.age)
        return 1
    return 0
  }
  
  console.log(personAges)