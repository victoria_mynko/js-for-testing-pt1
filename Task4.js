function doubleFactorial(c)
{
    if (c === 0  || c === 1)
      { return 1; }
    else
      { return c * doubleFactorial( c - 2 ); }
}

console.log(doubleFactorial(5))
